describe('Black Jack Functions', function() {
	describe('When a dealer has drawn two cards', function() {
		it('Expect dealerShouldDraw to return correct boolean', function() {
			// Define test cases
			const testCase1 = [{'suit':'clubs', 'val':10, 'displayVal':'10'},{'suit':'hearts', 'val':9, 'displayVal':'9'}];
			const testCase2 = [{'suit':'clubs', 'val':11, 'displayVal':'Ace'},{'suit':'hearts', 'val':6, 'displayVal':'6'}];
			const testCase3 = [{'suit':'clubs', 'val':10, 'displayVal':'10'},{'suit':'hearts', 'val':7, 'displayVal':'7'}];
			const testCase4 = [{'suit':'clubs', 'val':2, 'displayVal':'2'},{'suit':'hearts', 'val':4, 'displayVal':'4'},{'suit': 'spades', 'val':2, 'displayVal':'2'},{'suit':'diamonds', 'val':5, 'displayVal':'5'}];
			
			
			// Define test expectations
			expect(dealerShouldDraw(testCase1)).toBe(false);
			expect(dealerShouldDraw(testCase2)).toBe(true);
			expect(dealerShouldDraw(testCase3)).toBe(false);
			expect(dealerShouldDraw(testCase4)).toBe(true);
		})
	})
})