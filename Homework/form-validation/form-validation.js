// Function to create additional fields to collect more data on reason for contacting
const additionalItem = function (e) {
	$this = $(this);
	$reasonFormContainer = $this.parent().parent();
	let reasonFormContainer = $reasonFormContainer;

	// Checks to see if a reason has been added
	if ($reasonFormContainer.children().last().is('#talk-code')) {
		$reasonFormContainer.append('<div>');
	} else {
		$reasonFormContainer.children().last().empty();
	}

	$additionalItemContainer = $reasonFormContainer.children().last();
	$additionalItemContainer.attr('id','additional-item');

	// Toggles between selection-specific additional items
	if ($this.parent().is('#job-opportunity')) {
		$additionalItemContainer.append('<label>Job Title: </label>');
		$additionalItemContainer.append('<input>');
		$additionalItemContainer.children().last().attr('type','text');
		$additionalItemContainer.append('<label>Company Website: </label>');
		$additionalItemContainer.append('<input>');
		$additionalItemContainer.children().last().attr('type','url');
	} else {
		$additionalItemContainer.append('<label>Coding Language: </label>');
		$additionalItemContainer.append('<input>');
		$additionalItemContainer.children().last().attr('type','text');
		$additionalItemContainer.children().last().attr('id','language');
	}
}

// Function to check if the name meets requirement
const nameCheck = function (e) {
	$this = $(this);
	$nameField = $this.parent().parent().children().first().children()[1];
	$errorField = $this.parent().parent().children().first().children()[2];
	const errorField = $errorField;
	const nameField = $nameField;
	if (!nameField.validity.valid) {
		errorField.innerHTML = 'Required';
		e.preventDefault();
	} else if (nameField.validity.valid && nameField.value.length < 3) {
		errorField.innerHTML = 'Need to be at least 3 characters long';
		e.preventDefault();
	} else {
		errorField.innerHTML = '';
	}
}

// Function to check if the email address meets requirement
const emailCheck = function (e) {
	$this = $(this);
	$emailField = $this.parent().parent().children().first().next().children()[1];
	$errorField = $this.parent().parent().children().first().next().children()[2];
	const emailField = $emailField;
	const errorField = $errorField;
	const regex = /^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,}$/gmi;
	if (!emailField.value.match(regex)) {
		errorField.innerHTML = 'This is not a valid email address';
		e.preventDefault();
	} else {
		errorField.innerHTML = '';
	}
}

// Function to check if the message area meets requirement
const messageCheck = function (e) {
	$this = $(this);
	$messageField = $this.parent().parent().children().last().prev().children()[2];
	$errorField = $this.parent().parent().children().last().prev().children()[3];
	const messageField = $messageField;
	const errorField = $errorField;
	if(!messageField.validity.valid) {
		errorField.innerHTML = 'Required';
		e.preventDefault();
	} else if (messageField.validity.valid && messageField.value.length < 10) {
		errorField.innerHTML = 'Must be at least 10 characters';
		e.preventDefault();
	} else {
		errorField.innerHTML = '';
	}
}

// Function to set contact type in local storage based on contact reason
const setContactType = function(e) {
	const contactReason = document.getElementsByClassName('reason');
	let contactReasonValue;
	for (let i = 0; i < contactReason.length; i++) {
		if (contactReason[i].checked) {
			contactReasonValue = contactReason[i].nextElementSibling.innerText;
		}
	}
	localStorage.setItem('contact_type',contactReasonValue);
}

// Event Listener upon selecting contact reason
const reasonArray = document.getElementsByClassName('reason');
for (let i = 0; i < reasonArray.length; i++) {
	reasonArray[i].addEventListener('click', additionalItem);
}

// Event Listener to validate requirements upon submitting
const submitButton = document.getElementsByClassName('btn')[0];
submitButton.addEventListener('click', nameCheck);
submitButton.addEventListener('click', emailCheck);
submitButton.addEventListener('click', messageCheck);
submitButton.addEventListener('click', setContactType);