/**
 * Validates an individual input on form submit
 * @param {HTMLElement} inputEl 
 * @param {Event} submitEvent 
 */
const validateName = function(inputEl, submitEvent) {
	const labelEl = inputEl.parentElement.querySelector('label');
	const errorEl = inputEl.parentElement.querySelector('.error');
	if (!inputEl.validity.valid) {
		if (!inputEl.value) {
			errorEl.innerHTML = `${labelEl.innerText} is Required`;

		} else {
			errorEl.innerHTML = `${labelEl.innerText} is Not Valid`;
		}
		errorEl.classList.add('d-block');

		// Prevent form submit
		submitEvent.preventDefault();
	} else if (inputEl.validity.valid && inputEl.value.length < 3) {
		errorEl.innerHTML = `${labelEl.innerText} needs to be at least 3 characters long.`;
		submitEvent.preventDefault();
	} else {
		errorEl.innerHTML = '';
		errorEl.classList.remove('d-block');
	}
}

const validateEmail = function(inputEl, submitEvent) {
	const labelEl = inputEl.parentElement.querySelector('label');
	const errorEl = inputEl.parentElement.querySelector('.error');
	const regex = /\w+@\w+\.\w+/i;
	if (!inputEl.value.match(regex)) {
		errorEl.innerHTML = `${labelEl.innerText} is not a valid email address`;
	} else {
		errorEl.innerHTML = '';
		errorEl.classList.remove('d-block');
	}
}


const inputElements = document.getElementsByClassName('validate-input');
const formEl = document.getElementById('connect-form')
.addEventListener('submit', function(e) {
	for (let i = 0; i < inputElements.length; i++) {
		if (i <= 1) {
			validateName(inputElements[i], e);
		} else {
			validateEmail(inputElements[i],e);
		}
	}

	e.preventDefault();
});