// IN-CLASS EXERCISE #1
describe('Black Jack Functions', function() {
	describe('Given a set of cards', function() {
		it('calcPoints to correctly calculate the total points', function() {
			// Define test cases
			const testCase1 = [
				{'suit': 'spades',
					'val': 10,
					'displayVal': 'Jack'
				},
				{'suit': 'hearts',
					'val': 7,
					'displayVal': '7'}
			];
			const testCase2 = [
				{'suit': 'diamonds',
					'val': 11,
					'displayVal': 'Ace'},
				{'suit': 'clubs',
					'val': 9,
					'displayVal': '9'}
			];
			const testCase3 = [
				{'suit': 'clubs',
					'val': 10,
					'displayVal': '10'},
				{'suit': 'hearts',
					'val': 6,
					'displayVal': '6'},
				{'suit': 'spades',
					'val': 1,
					'displayVal': 'Ace'}
			];
			
			// Define test expectations
			expect(calcPoints(testCase1).total).toEqual(17);
			expect(calcPoints(testCase1).isSoft).toBe(false);
			expect(calcPoints(testCase2).total).toEqual(20);
			expect(calcPoints(testCase2).isSoft).toBe(true);
			expect(calcPoints(testCase3).total).toEqual(17);
			expect(calcPoints(testCase3).isSoft).toBe(false);
			})
	})
})